
________________________________________________________________________________
                            
                             LDAP AD Address Book
________________________________________________________________________________

Maintainer: 
 * Riccardo Mariani (richie_m), riccardo_mariani@ymail.com
 
________________________________________________________________________________

Description:

-> The address book module allows to connect to your AD through LDAP and 
   configure a page that implements the Address Book.


Installation:

 -> Copy the whole address_book directory to your modules directory and
    activate the module.

   
Usage:

 -> First of all read the module help 
 -> Go to admin/config/search/address_book/settings 
    and set the right parameters.
 -> Enter your path/address_book for show your address book page
________________________________________________________________________________
