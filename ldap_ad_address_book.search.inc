<?php
/**
 * @file
 * Implents the search functions in LDAP in this file.
 */

/**
 * Function for fill selects in main page.
 */
function ldap_ad_address_book_fill_select() {
  $select_fill_n = array('' => '');
  $arr_select = array('' => '');
  $num_select = variable_get('ldap_ad_address_book_howmany_select', 'no select');
  $ldap_host = variable_get('ldap_ad_address_book_ldap1_ip', 'default') . " "
  . variable_get('ldap_ad_address_book_ldap2_ip', 'default');
  $ldap_user  = variable_get('ldap_ad_address_book_ldapuser', 'default');
  $ldap_pass  = variable_get('ldap_ad_address_book_ldappass', 'default');
  $bind = TRUE;
  try {
    $ldap_conn = ldap_connect($ldap_host)  or die("Could not connect to {$ldap_host}");
    ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
    if ($ldap_conn) {
      $ldap_bind = ldap_bind($ldap_conn, $ldap_user, $ldap_pass);
      if ($ldap_bind) {
        for ($i = 1; $i <= $num_select; $i++) {
          $select_fill_n[$i] = ldap_ad_address_book_query_select($ldap_conn, $i);
        }
      }
      else {
        throw new Exception('Error binding');
      }
    }
    else {
      throw new Exception('Connection error');
    }
    ldap_unbind($ldap_conn);
  }
  catch (Exception $e) {
    $bind = FALSE;
    $arr_select = 'error binding';
  }
  if ($bind) {
    for ($i = 1; $i <= $num_select; $i++) {
      $fill_right = 0;
      $returns = '';
      $filter_search = variable_get("ldap_ad_address_book_ldap_select$i", '');
      foreach ($select_fill_n[$i] as $value) {
        for ($k = 0; $k < count($value); $k++) {
          if (isset($value[$k][$filter_search][0])) {
            $fill_right++;
            $returns[$fill_right] = $value[$k][$filter_search][0];
          }
        }
      }
      $returns[0] = '';
      sort($returns);
      $returns = drupal_map_assoc($returns);
      $arr_select[$i] = $returns;
    }
  }
  return $arr_select;
}
/**
 * Function query for fill select.
 */
function ldap_ad_address_book_query_select($ldap_conn, $numb_select_dev) {
  $conn_arr = array();
  $info = array();
  $arr_base_dn = variable_get("ldap_ad_address_book_dn_select$numb_select_dev", '');
  $filter_search = variable_get("ldap_ad_address_book_ldap_select$numb_select_dev", '');
  $ldap_filter = "(&(objectClass=User)(objectCategory=Person)(" . $filter_search . "=*)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))";
  $just_these = array($filter_search);
  for ($i = 0; $i < count($arr_base_dn); $i++) {
    $conn_arr[$i] = $ldap_conn;
  }
  try {
    $sr = ldap_search($conn_arr, $arr_base_dn, $ldap_filter, $just_these);
    ldap_get_option($ldap_conn, LDAP_OPT_ERROR_STRING, $error);
    if ($sr) {
      for ($i = 0; $i < count($sr); $i++) {
        $info[$i] = ldap_get_entries($ldap_conn, $sr[$i]);
      }
    }
  }
  catch (Exception $e) {
    drupal_set_message(t("something goes wrong in LDAP search"), 'error');
  }

  return $info;
}

/**
 * Submit handlet.
 */
function ldap_ad_address_book_all_submit($form, &$form_state) {
  $img_url = drupal_get_path('module', 'ldap_ad_address_book');
  $out = '';
  $element = $form['res']['show'];
  $select_num = variable_get('ldap_ad_address_book_howmany_select', 'no select');
  $text_num = variable_get('ldap_ad_address_book_howmany_text', 'no textfield');
  // Control that in the form there is one value at least (validation).
  $full = FALSE;
  $selected = '';
  $insert = '';
  $ldap_filter = '(&(objectClass=User)(objectCategory=Person)';
  for ($k = 1; $k <= $select_num; $k++) {
    $selected = $form_state['values']["select_page$k"];
    if (!empty($selected)) {
      $full = TRUE;
      $ldap_filter .= '(' .
      variable_get("ldap_ad_address_book_ldap_select$k", '') . '=' .
      $selected . ')';
    }
  }
  for ($k = 1; $k <= $text_num; $k++) {
    $insert = $form_state['values']["textfield_page$k"];
    if (!empty($insert) && ((strpos($insert, '*')) === FALSE)) {
      $full = TRUE;
      $ldap_filter .= '(' .
      variable_get("ldap_ad_address_book_ldap_textfield$k", '') . '='
      . $insert . ')';
    }
  }
  $ldap_filter .= '(!(userAccountControl:1.2.840.113556.1.4.803:=2)))';
  if ($full) {
    $count = 0;
    $base_dn_arr = array();
    $conn_arr = array();
    $just_these = array(
      'givenname',
      'sn',
      'mail',
      'telephonenumber',
      'mobile',
      'physicaldeliveryofficename',
      'department',
      'wWWHomePage',
    );
    $ldap_host = variable_get('ldap_ad_address_book_ldap1_ip', 'default') . " " .
    variable_get('ldap_ad_address_book_ldap2_ip', 'default');
    $ldap_user  = variable_get('ldap_ad_address_book_ldapuser', 'default');
    $ldap_pass  = variable_get('ldap_ad_address_book_ldappass', 'default');
    try {
      $ldap_conn = ldap_connect($ldap_host)
      or die("Could not connect to {$ldap_host}");
      ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
      ldap_set_option($ldap_conn, LDAP_OPT_REFERRALS, 0);
      if ($ldap_conn) {
        $ldap_bind = ldap_bind($ldap_conn, $ldap_user, $ldap_pass);
        if ($ldap_bind) {
          $how_many_dn = variable_get('ldap_ad_address_book_howmany_dn', '');
          for ($num_base = 1; $num_base <= $how_many_dn; $num_base++) {
            $base_dn_arr[$count] = variable_get("ldap_ad_address_book_param_base_dn$num_base", '');
            $conn_arr[$count] = $ldap_conn;
            $count++;
          }
          $sr = ldap_search($conn_arr, $base_dn_arr, $ldap_filter, $just_these);
          ldap_get_option($ldap_conn, LDAP_OPT_ERROR_STRING, $error);
          if ($sr) {
            for ($i = 0; $i < count($sr); $i++) {
              ldap_sort($ldap_conn, $sr[$i], 'sn');
              $res[$i] = ldap_get_entries($ldap_conn, $sr[$i]);
            }
          }
          else {
            $out .= "search error";
          }
        }
        else {
          throw new Exception('Error binding');
        }
      }
      else {
        throw new Exception('Connection error');
      }
      ldap_unbind($ldap_conn);
    }
    catch (Exception $e) {
      $out .= 'error binding';
    }
    for ($i = 0; $i < count($res); $i++) {
      $out .= ldap_ad_address_book_build_results($res[$i]);
    }
    switch ($out) {
      case 'error binding':
        $out .= '<div id="addres_result" style="float:left; margin-right:5px;">
        <img src="' . $img_url . '/img/error.png" alt="no value"></div>';
        $out .= 'error binding';
        break;

      case "":
        $out .= '<div id="addres_result" style="float:left; margin-right:5px;">
        <img src="' . $img_url . '/img/not_found.png" alt="not found"></div>';
        $out .= 'user not found';
        break;

    }
  }
  else {
    // If there isen't a value in the form print this.
    $out .= '<div id="addres_result" style="float:left; margin-right:5px;">
    <img src="' . $img_url . '/img/error.png" alt="no value"></div>';
    $out .= 'ENTER A VALUE';
  }
  $element['#markup'] = $out;
  return $element;
}

/**
 * Build result with html.
 */
function ldap_ad_address_book_build_results($info) {
  $page = '';
  $img_url = drupal_get_path('module', 'ldap_ad_address_book');
  $res_options = variable_get('ldap_ad_address_book_res_selected', '');
  if ($info["count"] != 0) {
    for ($i = 0; $i < $info["count"]; $i++) {
      foreach ($res_options as $key => $value) {
        switch ($res_options[$key]) {
          case 'displayname':
            $page .= '<div id="addres_result" style="float:left;
             margin-right:5px;"><img src="' . $img_url . '/img/user.png"
             alt="complete name"></div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]["givenname"][0]) && empty($info[$i]["sn"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["givenname"][0] . ' ' .
               $info[$i]["sn"][0] . "</div>";
            }
            break;

          case 'mail':
            $page .= '<div id="addres_result" style="float:left;
             margin-right:5px;"><img src="' . $img_url . '/img/mail.png"
             alt="email"> </div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]['mail'][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= '<a href="mailto:' . strtolower($info[$i]["mail"][0]) . '"
              target="_top">' . strtolower($info[$i]["mail"][0]) . '</a>
              </div> ';
            }
            break;

          case 'telephonenumber':
            $page .= '<div id="addres_result" style="float:left;
              margin-right:5px;"><img src="' . $img_url . '/img/phone.png"
              alt="phone"> </div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]["telephonenumber"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["telephonenumber"][0] . '</div>';
            }
            break;

          case 'mobile':
            $page .= '<div id="addres_result" style="float:left;
             margin-right:5px;">
            <img src="' . $img_url . '/img/mobile.png" alt="mobile"> </div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]["mobile"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["mobile"][0] . '</div>';
            }
            break;

          case 'physicaldeliveryofficename':
            $page .= '<div id="addres_result" style="float:left;
             margin-right:5px;"><img src="' . $img_url . '/img/department.png"
             alt="department"> </div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]["physicaldeliveryofficename"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["physicaldeliveryofficename"][0] . '</div>';
            }
            break;

          case 'department':
            $page .= '<div id="addres_result" style="float:left; 
             margin-right:5px;"><img src="' . $img_url . '/img/office.png"
             alt="office" width="32" height="32"> </div>
            <div id="addres_result" style="line-height:40px;
            vertical-align:middle;">';
            if (empty($info[$i]["department"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["department"][0] . '</div>';
            }
            break;

          case 'wWWHomePage':
            $page .= '<div id="addres_result" style="float:left;
             margin-right:5px;"><img src="' . $img_url . '/img/web.png"
             alt="web site"></div>
            <div id="addres_result" style="line-height:40px; vertical-align:middle;">';
            if (empty($info[$i]["wWWHomePage"][0])) {
              $page .= ' not available </div>';
            }
            else {
              $page .= $info[$i]["wWWHomePage"][0] . "</div>";
            }
            break;

        }
      }
      $page .= '<hr>';
    }
  }
  return $page;
}
