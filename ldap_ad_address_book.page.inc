<?php
/**
 * @file
 * Page file with form submission and markup for results.
 */

module_load_include('inc', 'ldap_ad_address_book', 'ldap_ad_address_book.search');
/**
 * Implements address book page.
 */
function ldap_ad_address_book_all($form, &$form_state) {
  $num_select = variable_get('ldap_ad_address_book_howmany_select', 'no select');
  $num_textfield = variable_get('ldap_ad_address_book_howmany_text', 'no textfield');
  $arr_select_fill = array('' => '');
  if ($num_select == 'no select') {
    $num_select = 0;
  }
  if ($num_textfield == 'no textfield') {
    $num_textfield = 0;
  }
  $form['address_book_page'] = array(
    '#type' => 'fieldset',
  );
  if ($num_select > 0) {
    $arr_select_fill = ldap_ad_address_book_fill_select();
    if ($arr_select_fill == 'error binding') {
      drupal_set_message(t("something goes wrong in your LDAP connection can't fill selects"), 'error');
    }
    else {
      for ($i = 1; $i <= $num_select; $i++) {
        $form['address_book_page']["select_page$i"] = array(
          '#type' => 'select',
          '#title' => variable_get("ldap_ad_address_book_labelselect$i", ''),
          '#options' => $arr_select_fill[$i],
          '#default_value' => '',
        );
      }
    }
  }
  if ($num_textfield > 0) {
    for ($i = 1; $i <= $num_textfield; $i++) {
      $form['address_book_page']["textfield_page$i"] = array(
        '#type' => 'textfield',
        '#title' => variable_get("ldap_ad_address_book_labeltext$i", ''),
        '#default_value' => '',
      );
    }
  }
  // Ajax submit.
  $form['address_book_page']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#ajax' => array(
      'callback' => 'ldap_ad_address_book_all_submit',
      'wrapper' => 'results',
      'event' => 'click',
    ),
  );
  $form['res']['show'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="results">',
    '#markup' => '',
    '#suffix' => '</div>',
  );
  return $form;
}
