<?php
/**
 * @file
 * Admin configuration form.
 */

define("MAX_SEL", 5);
define("MAX_TEXTFIELD", 8);
define("MAX_DN", 3);

/**
 * Form buildind.
 */
function ldap_ad_address_book_admin_settings($form, &$form_state) {
  $num_select = !empty($form_state['values']['howmany_select']) ? $form_state['values']['howmany_select'] : variable_get('ldap_ad_address_book_howmany_select', 'no select');
  if ($num_select == 'no select') {
    $num_select = 0;
  }
  $num_campi_testo = !empty($form_state['values']['howmany_text']) ? $form_state['values']['howmany_text'] : variable_get('ldap_ad_address_book_howmany_text', 'no textfield');
  if ($num_campi_testo == 'no textfield') {
    $num_campi_testo = 0;
  }
  $num_dn = !empty($form_state['values']['howmany_dn']) ? $form_state['values']['howmany_dn'] : variable_get('ldap_ad_address_book_howmany_dn', '1');
  $form['#attached']['css'][] = drupal_get_path('module', 'ldap_ad_address_book') .
   '/admin_css/admin_css.css';
  $form['address_book_conf'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['ldap_conf_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 1: LDAP configuration'),
    '#collapsible' => TRUE,
    '#description' => t('Enter the ldap configuration values'),
    '#group' => 'address_book_conf',
  );
  $form['ldap_conf_set']['ldap1_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP primary Host'),
    '#default_value' => variable_get('ldap_ad_address_book_ldap1_ip', ''),
    '#size' => 100,
    '#maxlength' => 100,
  );
  $form['ldap_conf_set']['ldap2_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP secondary Host'),
    '#default_value' => variable_get('ldap_ad_address_book_ldap2_ip', ''),
    '#size' => 100,
    '#maxlength' => 100,
  );
  $form['ldap_conf_set']['ldapuser'] = array(
    '#type' => 'textfield',
    '#title' => t('LDAP username'),
    '#default_value' => variable_get('ldap_ad_address_book_ldapuser', ''),
    '#size' => 100,
    '#maxlength' => 100,
  );
  $form['ldap_conf_set']['ldappass'] = array(
    '#type' => 'password',
    '#title' => t('LDAP password'),
    '#default_value' => variable_get('ldap_ad_address_book_ldappass', ''),
    '#size' => 100,
    '#maxlength' => 100,
  );
  $form['ldap_conf_set']['howmany_dn']  = array(
    '#title' => t('Base DN(s) number'),
    '#type' => 'select',
    '#options' => array(1 => 1, 2 => 2, 3 => 3),
    '#default_value' => $num_dn,
    '#ajax' => array(
      'callback' => 'ldap_ad_address_book_autobasedn_callback',
      'wrapper' => 'base_dn-div',
      'effect' => 'slide',
      'progress' => array('type' => 'none'),
    ),
  );
  $form['ldap_conf_set']['base_dn'] = array(
    '#title' => t('Insert DN for search'),
    '#prefix' => '<div id="base_dn-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
  );
  for ($i = 1; $i <= $num_dn; $i++) {
    $form['ldap_conf_set']['base_dn']["param_base_dn$i"] = array(
      '#type' => 'textfield',
      '#title' => t('insert dn') . $i,
      '#default_value' => variable_get("ldap_ad_address_book_param_base_dn$i", ''),
    );
  }
  $form['ldap_conf_set']['submit_one'] = array(
    '#type' => 'submit',
    '#value' => t('Save and test LDAP'),
    '#validate' => array('ldap_ad_address_book_admin_settings_validate_one'),
    '#submit' => array('ldap_ad_address_book_admin_submit_one'),
  );
  $form['setting_page_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 2: Form configuration'),
    '#collapsible' => TRUE,
    '#description' => t('Enter the values that you want to display in the address book page and the relatives LDAP parameters'),
    '#group' => 'address_book_conf',
  );
  $form['setting_page_set']['page_name'] = array(
    '#type' => 'textfield',
    '#title' => t('insert page name'),
    '#default_value' => variable_get('ldap_ad_address_book_page_name', ''),
    '#description' => t('enter the name of the page that you want to display'),
  );
  $form['setting_page_set']['howmany_select'] = array(
    '#title' => t('How many select(s) you need?'),
    '#type' => 'select',
    '#options' => array(
      'no select' => t('no select'),
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
    ),
    '#default_value' => $num_select,
    '#description' => t('view the help page for more details'),
    '#ajax' => array(
      'callback' => 'ldap_ad_address_book_autocheckboxes_callback',
      'wrapper' => 'select-div',
      'effect' => 'slide',
      'progress' => array('type' => 'none'),
    ),
  );
  $form['setting_page_set']['textfield_fieldset'] = array(
    '#title' => 'SELECT',
    '#prefix' => '<div id="select-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
  );
  $def_dn_select = array('' => '');
  $dn_choice = array('' => '');
  $dn_choice = ldap_ad_address_book_dn_array();
  for ($i = 1; $i <= $num_select; $i++) {
    $def_dn_select[$i] = ldap_ad_address_book_def_value_multiple_select($i);
    $form['setting_page_set']['textfield_fieldset']["labelselect$i"] = array(
      '#type' => 'textfield',
      '#title' => "name of the label for select $i",
      '#default_value' => variable_get("ldap_ad_address_book_labelselect$i", ''),
      '#prefix' => '<div id="labelselect" class="selects">',
      '#suffix' => '</div>',
    );
    $form['setting_page_set']['textfield_fieldset']["dn_select$i"] = array(
      '#type' => 'select',
      '#multiple' => 'TRUE',
      '#title' => t("dn where you want to search"),
      '#default_value' => $def_dn_select[$i],
      '#options' => $dn_choice,
      '#prefix' => '<div id="basedn" class="selects">',
      '#suffix' => '</div>',
      '#description' => t('multiple select with ctrl+right click in 
       Linux or Windows, cmd+right click in mac'),
    );
    $form['setting_page_set']['textfield_fieldset']["ldap_select$i"] = array(
      '#type' => 'textfield',
      '#title' => "LDAP parameter for fill the select $i",
      '#default_value' => variable_get("ldap_ad_address_book_ldap_select$i", ''),
      '#prefix' => '<div id="ldap_select" class="selects">',
      '#suffix' => '</div><div style="clear:both;"> &nbsp;</div>',
      '#description' => t('for example "department" for show all departments'),
    );
  }
  $form['setting_page_set']['howmany_text'] = array(
    '#title' => t('How many textfield(s) you need?'),
    '#type' => 'select',
    '#options' => array(
      'no textfield' => t('no textfield'),
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
    ),
    '#default_value' => $num_campi_testo,
    '#ajax' => array(
      'callback' => 'ldap_ad_address_book_autotext_callback',
      'wrapper' => 'textfield-div',
      'effect' => 'slide',
      'progress' => array('type' => 'none'),
    ),
    '#description' => t('view the help page for more details'),
  );
  $form['setting_page_set']['campi_testo'] = array(
    '#title' => t("TEXTFIELD"),
    '#prefix' => '<div id="textfield-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
  );
  for ($k = 1; $k <= $num_campi_testo; $k++) {
    $form['setting_page_set']['campi_testo']["labeltext$k"] = array(
      '#type' => 'textfield',
      '#title' => "name of the label for textfield $k",
      '#default_value' => variable_get("ldap_ad_address_book_labeltext$k", ''),
      '#prefix' => '<div id="labeltext" class="selects">',
      '#suffix' => '</div>',
    );
    $form['setting_page_set']['campi_testo']["ldap_textfield$k"] = array(
      '#type' => 'textfield',
      '#title' => t("parameter for build the filter of the LDAP query"),
      '#default_value' => variable_get("ldap_ad_address_book_ldap_textfield$k", ''),
      '#prefix' => '<div id="textfield" class="selects">',
      '#suffix' => '</div> <div style="clear:both;"> &nbsp;</div>',
      '#description' => t('for example "givenname" or "sn"'),
    );
  }
  $form['setting_page_set']['submit_two'] = array(
    '#type' => 'submit',
    '#value' => t('Save Form config'),
    '#validate' => array('ldap_ad_address_book_admin_settings_validate_two'),
    '#submit' => array('ldap_ad_address_book_admin_submit_two'),
  );
  $form['result_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 3: Results configuration'),
    '#collapsible' => TRUE,
    '#description' => t('Select what you want to show in the results'),
    '#group' => 'address_book_conf',
  );
  $form['result_set']['res_options'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'displayname' => t('name and surname'),
      'mail' => t('e-mail address'),
      'telephonenumber' => t('phone number'),
      'mobile' => t('mobile number'),
      'physicaldeliveryofficename' => t('office'),
      'department' => t('department'),
      'wWWHomePage' => t('web page address'),
    ),
    '#default_value' => variable_get('ldap_ad_address_book_res_selected', ''),
    '#prefix' => '<br /><hr />',
    '#suffix' => '<hr /><br />',
  );
  $form['result_set']['submit_three'] = array(
    '#type' => 'submit',
    '#value' => t('Save Results config'),
    '#validate' => array('ldap_ad_address_book_admin_settings_validate_three'),
    '#submit' => array('ldap_ad_address_book_admin_submit_three'),
  );

  return $form;
}

/**
 * Ajax callback for display base dn textfields.
 */
function ldap_ad_address_book_autobasedn_callback($form, $form_state) {
  return $form['ldap_conf_set']['base_dn'];
}

/**
 * Ajax callback for display selects.
 */
function ldap_ad_address_book_autocheckboxes_callback($form, $form_state) {
  return $form['setting_page_set']['textfield_fieldset'];
}

/**
 * Ajax callback for display textfields.
 */
function ldap_ad_address_book_autotext_callback($form, $form_state) {
  return $form['setting_page_set']['campi_testo'];
}

/**
 * Options value for dn multiple select.
 */
function ldap_ad_address_book_dn_array() {
  $dn_arr = array();
  $dn_number = variable_get('ldap_ad_address_book_howmany_dn', '');
  for ($i = 1; $i <= $dn_number; $i++) {
    $dn_arr[variable_get("ldap_ad_address_book_param_base_dn$i", '')] = variable_get("ldap_ad_address_book_param_base_dn$i", '');
  }
  return $dn_arr;
}

/**
 * Default value for dn multiple select.
 */
function ldap_ad_address_book_def_value_multiple_select($num_of_select) {
  $def_choice = array('' => '');
  $def_choice = variable_get("ldap_ad_address_book_dn_select$num_of_select", '');
  $c = 0;
  $err_no = '';
  foreach ($def_choice as $key => $value) {
    $default_choice[$c] = $value;
    $c++;
    $err_no = $key;
  }
  return $default_choice;
}

/**
 * Validation for the first part of the form.
 */
function ldap_ad_address_book_admin_settings_validate_one($form, $form_state) {
  $base_dn_err = FALSE;
  $how_many_dn = $form_state['values']['howmany_dn'];
  if ($form_state['values']['ldap1_ip'] != '0') {
    if (empty($form_state['values']['ldap1_ip'])) {
      form_set_error('ldap1_ip', t('missing primary ldap host'));
    }
  }
  if ($form_state['values']['ldapuser'] != '0') {
    if (empty($form_state['values']['ldapuser'])) {
      form_set_error('ldapuser', t('missing primary ldap user'));
    }
  }
  if ($form_state['values']['ldappass'] != '0') {
    if (empty($form_state['values']['ldappass'])) {
      form_set_error('ldappass', t('missing primary ldap password'));
    }
  }
  for ($i = 1; $i <= $how_many_dn; $i++) {
    if (empty($form_state['values']["param_base_dn$i"])) {
      $base_dn_err = TRUE;
      form_set_error("param_base_dn$i", "missing base dn $i");
    }
  }
  if ($base_dn_err) {
    form_set_error('howmany_dn', t('wrong dn number'));
  }
}

/**
 * Validation for the second part of the form, validate first too.
 */
function ldap_ad_address_book_admin_settings_validate_two($form, $form_state) {
  $base_dn_err = FALSE;
  $how_many_select = $form_state['values']['howmany_select'];
  $how_many_text = $form_state['values']['howmany_text'];
  $text_error = FALSE;
  $select_error = FALSE;
  for ($i = 1; $i <= $how_many_select; $i++) {
    if ($form_state['values']["labelselect$i"] != '0') {
      if (empty($form_state['values']["labelselect$i"])) {
        $select_error = TRUE;
        form_set_error("labelselect$i", "missing label $i select name");
      }
    }
    if ($form_state['values']["ldap_select$i"] != '0') {
      if (empty($form_state['values']["ldap_select$i"])) {
        $select_error = TRUE;
        form_set_error("ldap_select$i", "missing ldap $i select value");
      }
    }
    if (empty($form_state['values']["dn_select$i"])) {
      $select_error = TRUE;
      form_set_error("dn_select$i", "missing dn $i value");
    }
  }
  for ($k = 1; $k <= $how_many_text; $k++) {
    if (($form_state['values']["labeltext$k"] != '0') or $form_state['values']["ldap_textfield$k"] != '0') {
      if (empty($form_state['values']["labeltext$k"])) {
        $text_error = TRUE;
        form_set_error("labeltext$k", "missing label $i text name");
      }
    }
    if ($form_state['values']["ldap_textfield$k"] != '0') {
      if (empty($form_state['values']["ldap_textfield$k"])) {
        $text_error = TRUE;
        form_set_error("ldap_textfield$k", "missing ldap $i text value");
      }
    }
  }
  if ($text_error) {
    form_set_error('howmany_text', t('wrong textfields number'));
  }
  if ($select_error) {
    form_set_error('howmany_select', t('wrong selects number'));
  }
}

/**
 * Validation for the third part of the form, validate first too.
 */
function ldap_ad_address_book_admin_settings_validate_three($form, $form_state) {
  $base_dn_err = FALSE;
  $empty = TRUE;
  $err_no = '';
  foreach ($form_state['values']['res_options'] as $ldap_val => $show_val) {
    if ($show_val) {
      $err_no = $ldap_val;
      $empty = FALSE;
      break;
    }
  }
  if ($empty) {
    form_set_error('res_options', t('select at least one value'));
  }
}

/**
 * Submit for the first part of the form.
 */
function ldap_ad_address_book_admin_submit_one($form, $form_state) {
  variable_set('ldap_ad_address_book_ldap1_ip', $form_state['values']['ldap1_ip']);
  variable_set('ldap_ad_address_book_ldap2_ip', $form_state['values']['ldap2_ip']);
  variable_set('ldap_ad_address_book_ldapuser', $form_state['values']['ldapuser']);
  variable_set('ldap_ad_address_book_ldappass', $form_state['values']['ldappass']);
  $how_many_dn = $form_state['values']['howmany_dn'];
  $prev_dn_number = variable_get('ldap_ad_address_book_howmany_dn', '');
  if ($prev_dn_number > $how_many_dn) {
    variable_set('ldap_ad_address_book_howmany_dn', "$how_many_dn");
    for ($i = $how_many_dn + 1; $i <= MAX_DN; $i++) {
      variable_del("ldap_ad_address_book_param_base_dn$i");
    }
  }
  else {
    variable_set('ldap_ad_address_book_howmany_dn', "$how_many_dn");
  }
  for ($i = 1; $i <= $how_many_dn; $i++) {
    variable_set("ldap_ad_address_book_param_base_dn$i", $form_state['values']["param_base_dn$i"]);
  }
  drupal_set_message(t('LDAP configuration saved'), 'status');
  $ldap_host = variable_get('ldap_ad_address_book_ldap1_ip', 'default') . " " . variable_get('ldap_ad_address_book_ldap2_ip', 'default');
  $ldap_user  = variable_get('ldap_ad_address_book_ldapuser', 'default');
  $ldap_pass  = variable_get('ldap_ad_address_book_ldappass', 'default');
  try {
    $ldap_conn = ldap_connect($ldap_host)
    or die("Could not connect to {$ldap_host}");;
    ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ($ldap_conn) {
      $ldap_bind = ldap_bind($ldap_conn, $ldap_user, $ldap_pass);
      if ($ldap_bind) {
        drupal_set_message(t('connection to ldap work'), 'status');
      }
      else {
        throw new Exception('Error binding');
      }
    }
    else {
      throw new Exception('Connection error');
    }
  }
  catch (Exception $e) {
    drupal_set_message(t("something goes wrong in your LDAP connection"), 'error');
  }
  ldap_unbind($ldap_conn);
}

/**
 * Submit for the second part of the form.
 */
function ldap_ad_address_book_admin_submit_two($form, $form_state) {
  $how_many_select = $form_state['values']['howmany_select'];
  if ($how_many_select == 'no select') {
    $how_many_select = 0;
  }
  $how_many_text = $form_state['values']['howmany_text'];
  if ($how_many_text == 'no textfield') {
    $how_many_text = 0;
  }
  $prev_num_select = variable_get('ldap_ad_address_book_howmany_select', '');
  $prev_num_text = variable_get('ldap_ad_address_book_howmany_text', '');
  variable_set('ldap_ad_address_book_page_name', $form_state['values']['page_name']);
  if ($prev_num_select > $how_many_select) {
    variable_set('ldap_ad_address_book_howmany_select', "$how_many_select");
    for ($j = $how_many_select + 1; $j <= MAX_SEL; $j++) {
      variable_del("ldap_ad_address_book_ldap_select$j");
      variable_del("ldap_ad_address_book_labelselect$j");
      variable_del("ldap_ad_address_book_dn_select$j");
    }
  }
  else {
    variable_set('ldap_ad_address_book_howmany_select', $form_state['values']['howmany_select']);
  }
  if ($prev_num_text > $how_many_text) {
    variable_set('ldap_ad_address_book_howmany_text', "$how_many_text");
    for ($k = $how_many_text + 1; $k <= MAX_TEXTFIELD; $k++) {
      variable_del("ldap_ad_address_book_ldap_textfield$k");
      variable_del("ldap_ad_address_book_labeltext$k");
    }
  }
  else {
    variable_set('ldap_ad_address_book_howmany_text', $form_state['values']['howmany_text']);
  }
  for ($i = 1; $i <= $how_many_select; $i++) {
    variable_set("ldap_ad_address_book_labelselect$i", $form_state['values']["labelselect$i"]);
    variable_set("ldap_ad_address_book_ldap_select$i", $form_state['values']["ldap_select$i"]);
    variable_set("ldap_ad_address_book_dn_select$i", $form_state['values']["dn_select$i"]);
  }
  for ($k = 1; $k <= $how_many_text; $k++) {
    variable_set("ldap_ad_address_book_labeltext$k", $form_state['values']["labeltext$k"]);
    variable_set("ldap_ad_address_book_ldap_textfield$k", $form_state['values']["ldap_textfield$k"]);
  }
  menu_rebuild();
  drupal_set_message(t('Form configuration saved'), 'status');
}

/**
 * Submit for the third part of the form.
 */
function ldap_ad_address_book_admin_submit_three($form, $form_state) {
  $i = 0;
  $selected = array();
  foreach ($form_state['values']['res_options'] as $ldap_val => $show_val) {
    if ($show_val) {
      $selected[$i] = $ldap_val;
      $i++;
    }
  }
  variable_set("ldap_ad_address_book_res_selected", $selected);
  drupal_set_message(t('Results configuration saved'), 'status');
}
